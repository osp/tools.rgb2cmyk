var pdf = new PDFDocument(argv[1]);
pdf.getTrailer().Root.PageLayout = "TwoColumnLeft";
var n = pdf.countPages();
for (var i = 0; i < n; ++i) {
    var page = pdf.findPage(i);
    var currentWidth = page.MediaBox[2];
    var currentHeight = page.MediaBox[3];
    var wcrop = ((currentWidth - 482) / 2);
    var hcrop = ((currentHeight - 652) / 2);
    print(wcrop);
    page.MediaBox = page.CropBox = [wcrop,hcrop, 482 + wcrop, 652 + hcrop];
}
pdf.save(argv[2]);
