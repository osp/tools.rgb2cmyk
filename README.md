Quick start
===========

This is a set of scripts to convert RGB PDFs to CMYK, while preserving pure
black. At the moment it is optimized for the belgian quaterly Médor, but it
aims to become more generic. Meanwhile, you can explore it and tinker with it
to adapt it to your own needs.

Licence
-------

This file is part of HTML2print.

HTML2print is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

HTML2print is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with HTML2print.  If not, see <http://www.gnu.org/licenses/>.


Pre-requisites
--------------

In order for the scripts to work, make sure you have all the system-wide
dependencies:

- ghostscript
- pdftops
- mutools
- pdftk
- imagemagick


Using it
--------

Organize your PDF directory accordingly:

    .
    ├── NonOverprint
    ├── Overprint
    └── Scribus

- In NonOverprint, put the RGB PDF that DO need to be converted to CMYK;
- In Overprint, put the RGB PDF that DO need to be converted to CMYK, but with
  black overprinting;
- In Scribus, put the PDFs that DON'T need to be converted to CMYK.

Then, fill the directory with your PDFs. We prefix their names with the page
numbers to make sure they are assembled in the right order, like so:

    .
    ├── NonOverprint
    │   ├── 001-002-editorial.pdf
    │   └── 003-some-article.pdf
    ├── Overprint
    │   └── 004-005-table-of-contents.pdf
    └── Scribus
        └── 006-012-some-other-article.pdf

Move in this directory, and make a symbolic link to your PDFs, naming the link
`pdf`, like so:

   ln -s /path/to/your/PDF/directory/ pdf


type:

- `make cmyk` to generate all CMYK Pdfs. 
- `make pdf` to assemble the individual Pdfs
- `make` to run the two commands above at once.
- `make cahiers` to generate the four 32 pages booklets.


