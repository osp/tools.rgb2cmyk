# This file is part of HTML2print.
# 
# HTML2print is free software: you can redistribute it and/or modify it under the
# terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
# 
# HTML2print is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
# 
# You should have received a copy of the GNU Affero General Public License along
# with HTML2print.  If not, see <http://www.gnu.org/licenses/>.





# Avoids unexpected shell behaviors
SHELL := /usr/bin/env bash





#############
# Variables #
#############

OVERPRINT_INPUT = $(shell find pdf/Overprint -type f -name '*.pdf')
NONOVERPRINT_INPUT = $(shell find pdf/NonOverprint -type f -name '*.pdf')
SCRIBUS_INPUT = $(shell find pdf/Scribus -type f -name '*.pdf')

NONOVERPRINT_OUTPUT = $(patsubst pdf/NonOverprint/%, build/pdf/cmyk/%, $(NONOVERPRINT_INPUT))
OVERPRINT_OUTPUT = $(patsubst pdf/Overprint/%, build/pdf/cmyk/%, $(OVERPRINT_INPUT))
SCRIBUS_OUTPUT = $(patsubst pdf/Scribus/%, build/pdf/cmyk/%, $(SCRIBUS_INPUT))

NONOVERPRINT_LIGHT_OUTPUT = $(patsubst pdf/NonOverprint/%, build/pdf/light/%, $(NONOVERPRINT_INPUT))

ALL_CMYK = $(shell find build/pdf/cmyk -type f -name '*.pdf' | sort)





#############
# Shortcuts #
#############

all: pdf
nonoverprint: $(NONOVERPRINT_OUTPUT)
light: $(NONOVERPRINT_LIGHT_OUTPUT)
overprint: $(OVERPRINT_OUTPUT)
scribus: $(SCRIBUS_OUTPUT)
cmyk: nonoverprint overprint scribus
pdf: build/pdf/medor.pdf 
cahiers: build/pdf/cahier-1.pdf build/pdf/cahier-2.pdf build/pdf/cahier-3.pdf build/pdf/cahier-4.pdf





############
# Recipies #
############

build/pdf/light/%.pdf : pdf/NonOverprint/%.pdf
	mkdir -p $(@D)
	# /usr/bin/env gs  -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -sOutputFile=ce-monstre-en-moi_light.pdf ce-monstre-en-moi.pdf
	mutool run bin/resize.js $< $@


build/pdf/cmyk/%.pdf : pdf/NonOverprint/%.pdf
	mkdir -p $(@D)
	./bin/rgb2cmyk.sh -f $@ $<
	./bin/colorSeparation.sh $@ $<


build/pdf/cmyk/%.pdf : pdf/Overprint/%.pdf
	mkdir -p $(@D)
	./bin/rgb2cmyk.sh -f $@ -o $<
	./bin/colorSeparation.sh $@ $<


build/pdf/cmyk/%.pdf : pdf/Scribus/%.pdf
	mkdir -p $(@D)
	cp -f $< $@


build/pdf/medor.pdf : $(ALL_CMYK)
	mkdir -p $(@D)
	pdftk $^ cat output $@


build/pdf/cahier-1.pdf : build/pdf/medor.pdf
	mkdir -p $(@D)
	pdftk A=$^ cat A1-32 output $@


build/pdf/cahier-2.pdf : build/pdf/medor.pdf
	mkdir -p $(@D)
	pdftk A=$^ cat A33-64 output $@


build/pdf/cahier-3.pdf : build/pdf/medor.pdf
	mkdir -p $(@D)
	pdftk A=$^ cat A65-96 output $@


build/pdf/cahier-4.pdf : build/pdf/medor.pdf
	mkdir -p $(@D)
	pdftk A=$^ cat A97-128 output $@


# foo: build/pdf/medor.pdf
#     # without an escaped newline at the end, each line is run by separate (ba)sh -c ''
#     foo=`pdftk $^ dump_data | grep NumberOfPages | awk '{ print $$2 }'`; \
#     echo "$$foo"


.PHONY: debug
debug:
	@echo $(OVERPRINT_INPUT)
	@echo $(SCRIBUS_INPUT)
	@echo $(NONOVERPRINT_OUTPUT)
	@echo $(OVERPRINT_OUTPUT)
	@echo $(SCRIBUS_OUTPUT)
	@echo "ALL_CMYK:" $(ALL_CMYK)
	@echo "NONOVERPRINT_INPUT:" $(NONOVERPRINT_INPUT)
	@echo "NONOVERPRINT_LIGHT_OUTPUT:" $(NONOVERPRINT_LIGHT_OUTPUT)


.PHONY: clean
clean:
	rm -fr build
